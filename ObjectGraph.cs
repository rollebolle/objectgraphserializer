﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ObjectGraphSerializer
{    
    public class TypeValue
    {
        public string Name { get; set; }
        public Type Type { get; set; }
        public object Value { get; set; }
    }


    public class ObjectNode
    {
        public ObjectNode()
        {
            TypeValues = new List<TypeValue>();
        }

        public int RefId { get; set; }
        public Type Type { get; set; }
        public List<TypeValue> TypeValues { get; private set; }
    }


    public class ObjectGraph
    {
        public List<ObjectNode> Objects;
        private int refCounter;
        private readonly List<Tuple<int, object>> ReferenceInstances;
        private MethodChache methodCache;

        internal ObjectGraph()
        {
            Objects = new List<ObjectNode>();
            ReferenceInstances = new List<Tuple<int, object>>();
            methodCache = new MethodChache();
        }

        internal static ObjectGraph ConstructGraph(object obj)
        {
            var graph = new ObjectGraph();
            graph.Crawl(obj);
            return graph;
        }

        internal void Crawl(object obj, int refId = 0)
        {
            var root = new ObjectNode { RefId = refId, Type = obj.GetType() };
            Objects.Add(root);
            ReferenceInstances.Add(new Tuple<int, object>(refCounter, obj));
           
            if (obj is ISerializable)
            {
                // Let client handle serialization
                ((ISerializable)obj).Serialize(this, root);
                return;
            }

            foreach (var prop in obj.GetType().GetProperties())
            {
                var value = prop.GetValue(obj);
                if (prop.PropertyType.IsValueType || prop.PropertyType == typeof(string))
                {
                    root.TypeValues.Add(new TypeValue { Name = prop.Name, Type = prop.PropertyType, Value = value });
                }
                else if (typeof(IList).IsAssignableFrom(prop.PropertyType))
                {
                    // TODO: check if generic arg is value type or not
                    // special handling for lists
                    var enumerable = prop.GetValue(obj) as IEnumerable;
                    var typeValue = new TypeValue { Name = prop.Name, Type = prop.PropertyType, Value = new ReferenceList() };
                    foreach (var @enum in enumerable)
                    {
                        var reference = ReferenceInstances.FirstOrDefault(x => x.Item2 == @enum);
                        if (reference != null)
                        {
                            (typeValue.Value as ReferenceList).References.Add(reference.Item1);
                            continue;
                        }

                        ++refCounter;
                        (typeValue.Value as ReferenceList).References.Add(refCounter);
                        Crawl(@enum, refCounter);
                    }
                    root.TypeValues.Add(typeValue);
                }
                else if (typeof(IDictionary).IsAssignableFrom(prop.PropertyType))
                {
                    var dictionary = prop.GetValue(obj) as IDictionary;
                    var typeValue = new TypeValue { Name = prop.Name, Type = prop.PropertyType, Value = new ReferenceDictionary() };
                    foreach (var key in dictionary.Keys)
                    {
                        var reference = ReferenceInstances.FirstOrDefault(x => x.Item2 == dictionary[key]);
                        if (reference != null)
                        {

                            (typeValue.Value as ReferenceDictionary).References[key.ToString()] = reference.Item1;
                            continue;
                        }

                        ++refCounter;
                        (typeValue.Value as ReferenceDictionary).References[key.ToString()] = refCounter;
                        Crawl(dictionary[key], refCounter);
                    }
                    root.TypeValues.Add(typeValue);
                }
                else
                {
                    AddReferenceType(root, value, prop.Name, prop.PropertyType);
                }
            }
        }

        public void AddReferenceType(ObjectNode root, object value, string name, Type type)
        {
            // search if already added
            var reference = ReferenceInstances.FirstOrDefault(x => x.Item2 == value);
            if (reference != null)
            {
                root.TypeValues.Add(new TypeValue { Name = name, Type = type, Value = reference.Item1 });
            }
            else
            {
                // add new reference and create the new object by crawling it
                if (value == null)
                {
                    root.TypeValues.Add(new TypeValue { Name = name, Type = type, Value = null });
                    return;
                }
                ++refCounter;
                root.TypeValues.Add(new TypeValue { Name = name, Type = type, Value = refCounter });
                Crawl(value, refCounter);
            }
        }

        internal object DeconstructGraph()
        {
            var tmpObjs = new List<object>();
            // Pass 1, create all value type props
            foreach (var node in Objects)
            {
                var o = methodCache.InvokeParameterlessCtor(node.Type);                
                {
                    if (typeof(ISerializable).IsAssignableFrom(node.Type))
                        ((ISerializable)o).Deserialize(node);
                }

                foreach (var typeValue in node.TypeValues)
                {
                    if (typeValue.Type.IsValueType || typeValue.Type == typeof(string))
                        methodCache.Invoke(o.GetType(), "set_" + typeValue.Name, o, new object[] { typeValue.Value });
                        //o.GetType().GetProperty(typeValue.Name).SetValue(o, typeValue.Value);
                }

                tmpObjs.Add(o);
            }

            var i = 0;
            // Pass 2, create refs
            foreach (var node in Objects)
            {
                foreach (var typeValue in node.TypeValues)
                {
                    // not very effective but we need the property anyway later to find list/dict property types
                    var property = tmpObjs[i].GetType().GetProperty(typeValue.Name);
                    // special handling of lists
                    if (typeof(IList).IsAssignableFrom(typeValue.Type) && typeValue.Type != typeof(string))
                    {                        
                        var refList = (ReferenceList)typeValue.Value;
                        foreach (var refId in refList.References)
                        {                                                        
                            methodCache.Invoke(property.PropertyType, "Add", property.GetValue(tmpObjs[i]), new[] { tmpObjs[refId] });                            
                        }
                    }
                    else if (typeof(IDictionary).IsAssignableFrom(typeValue.Type))
                    {
                        var refDict = (ReferenceDictionary)typeValue.Value;
                        foreach (var entry in refDict.References)
                        {
                            methodCache.Invoke(property.PropertyType, "Add", property.GetValue(tmpObjs[i]), new[] { entry.Key, tmpObjs[entry.Value] });                            
                        }
                    }
                    else if (!typeValue.Type.IsValueType && typeValue.Type != typeof(string))
                    {
                        if (typeValue.Value == null)
                            continue;
                        var refId = typeValue.Value;                        
                        property.SetValue(tmpObjs[i], tmpObjs[(int)refId]);
                    }
                }
                ++i;
            }

            return tmpObjs[0];
        }

        // Cache for methods so we dont have to reflect them each time they are accessed
        public class MethodChache
        {
            private Dictionary<Type, Dictionary<string, MethodInfo>> cachedMethods = new Dictionary<Type,Dictionary<string,MethodInfo>>();
            private Dictionary<Type, ConstructorInfo> cachedConstructors = new Dictionary<Type,ConstructorInfo>();

            public object Invoke(Type type, string methodName, object obj, object[] @params)
            {
                if (!cachedMethods.ContainsKey(type))
                    cachedMethods[type] = new Dictionary<string, MethodInfo>();

                var methodDict = cachedMethods[type];
                
                if (!methodDict.ContainsKey(methodName))
                    methodDict[methodName] = type.GetMethod(methodName);

                return methodDict[methodName].Invoke(obj, @params);
            }

            public object InvokeParameterlessCtor(Type type)
            {
                if (!cachedConstructors.ContainsKey(type))
                    cachedConstructors[type] = type.GetConstructor(new Type[] { });

                return cachedConstructors[type].Invoke(new object[]{ });
                
            }
        }

        // Used to store a list of references
        public class ReferenceList
        {
            public List<int> References { get; private set; }
            
            public ReferenceList()
            {
                References = new List<int>();
            }

            public override string ToString()
            {
                var strBuilder = new StringBuilder();
                foreach (var @ref in References)
                {
                    strBuilder.Append(@ref);
                    strBuilder.Append(" ");
                }

                return strBuilder.ToString();
            }
        }

        public class ReferenceDictionary
        {
            public Dictionary<string, int> References { get; private set; }

            public ReferenceDictionary()
            {
                References = new Dictionary<string, int>();
            }

            public override string ToString()
            {
                var strBuilder = new StringBuilder();
                foreach (var @ref in References)
                {
                    strBuilder.Append(@ref.Key);
                    strBuilder.Append(",");
                    strBuilder.Append(@ref.Value);
                    strBuilder.Append(" ");
                }

                return strBuilder.ToString();
            }
        }
    }
}
