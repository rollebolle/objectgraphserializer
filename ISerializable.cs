﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectGraphSerializer
{
    public interface ISerializable
    {
        void Serialize(ObjectGraph objectGraph, ObjectNode currentObject);
        void Deserialize(ObjectNode node);
    }
}
