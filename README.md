General purpose object graph serializer. Unlike .NET's built-in serializer and the WCF
serializer, which are both tree serializer, this class persists the object graph. This means
that references are tracked and persisted correctly, and also that it handles object-type
references.
     
Also unlike most popular serializers, there is no need to mark classes and all thier subclasses
with attributes to indicate that they should be serialized. This obviously comes with a performance
hit as the full type name must be serialized.
      
Current limitations:
  - Only handles List and Dictionary collections, and Dictionary keys must be strings.
