﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectGraphSerializer.Tests
{
    class Base
    {
        public int MyProp { get; set; }
    }

    class Derived : Base
    {
        public int MyDerivedProp { get; set; }
    }

    class DerivedComposer
    {
        public Base MyStuff { get; set; }
    }

    class SimpleClass
    {
        public int IntProp { get; set; }
    }

    class ListClass
    {
        public ListClass()
        {
            Stuff = new List<SimpleClass>();
        }

        public List<SimpleClass> Stuff { get; private set; }
    }

    class DictionaryClass
    {
        public DictionaryClass()
        {
            Stuff = new Dictionary<string, SimpleClass>();
        }

        public Dictionary<string, SimpleClass> Stuff { get; private set; }
        public int AnInt { get; set; }
    }

    class DictionaryClass2
    {
        public DictionaryClass2()
        {
            Stuff = new Dictionary<int, SimpleClass>();
        }

        public Dictionary<int, SimpleClass> Stuff { get; private set; }
    }

    class CustomSerializationClass : ISerializable
    {
        public string KString { get; set; }
        public CustomSerializationClass KKlazz { get; set; }
        
        public void Serialize(ObjectGraph objectGraph, ObjectNode currentObject)
        {
            currentObject.TypeValues.Add(new TypeValue
            {
                Name = "KString",
                Type = typeof(string),
                Value = KString
            });

            objectGraph.AddReferenceType(currentObject, KKlazz, "KKlazz", typeof(CustomSerializationClass));
        }

        public void Deserialize(ObjectNode node)
        {
            KString = (string)node.TypeValues.First(typeValue => typeValue.Name == "KString").Value;
            // Observe that refs not needed to be fixed here
        }
    }
}
