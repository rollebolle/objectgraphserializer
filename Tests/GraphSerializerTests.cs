﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace ObjectGraphSerializer.Tests
{
    class GraphSerializerTests
    {
        [Test]
        public void DerivedClassesTest()
        {
            var input = new DerivedComposer()
            {
                MyStuff = new Derived()
                {
                    MyDerivedProp = 1, 
                    MyProp = 2
                }
            };

            DerivedComposer output;
            using (var stream = new MemoryStream())
            {
                GraphSerializer.Serialize(input, stream);
                stream.Position = 0;
                output = GraphSerializer.Deserialize<DerivedComposer>(stream);
            }

            Assert.NotNull(output);
            Assert.That(output.MyStuff.MyProp == 2);

            var derived = output.MyStuff as Derived;
            Assert.NotNull(derived);
            Assert.That(derived.MyDerivedProp == 1);
            
            
        }

        [Test]
        public void DictionaryTest()
        {
            var input = new DictionaryClass();
            input.Stuff["foo"] = new SimpleClass() { IntProp = 1 };
            input.Stuff["bar"] = new SimpleClass() { IntProp = 2 };

            DictionaryClass output = null;
            using (var stream = new MemoryStream())
            {
                GraphSerializer.Serialize(input, stream);
                stream.Position = 0;
                output = GraphSerializer.Deserialize<DictionaryClass>(stream);
            }

            Assert.NotNull(output);
            Assert.That(output.Stuff["foo"].IntProp == 1);
            Assert.That(output.Stuff["bar"].IntProp == 2);
        }
    }
}
