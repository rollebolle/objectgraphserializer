﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace ObjectGraphSerializer.Tests
{
    [TestFixture]
    class ObjectGraphTests
    {
        
        [SetUp]
        public void Setup()
        {
            
        }

        [Test]
        public void DerivedClassesTest()
        {
            var input = new DerivedComposer()
            {
                MyStuff = new Derived()
                {
                    MyDerivedProp  = 1,
                    MyProp = 2
                }
            };

            var output = ConstructAndDeconstruct<DerivedComposer>(input);
            
            Assert.NotNull(output);
            Assert.That(output.MyStuff.MyProp == 2);

            var derived = output.MyStuff as Derived;
            Assert.NotNull(derived);
            Assert.That(derived.MyDerivedProp == 1);
        }

        [Test]
        public void ListTest()
        {
            var input = new ListClass();
            input.Stuff.Add(new SimpleClass() { IntProp = 1 });
            input.Stuff.Add(new SimpleClass() { IntProp = 2 });

            var output = ConstructAndDeconstruct<ListClass>(input);

            Assert.NotNull(output);
            Assert.That(output.Stuff[0].IntProp == 1);
            Assert.That(output.Stuff[1].IntProp == 2);
        }

        [Test]
        public void ReferencesAreKeptTest()
        {
            var input = new ListClass();
            var simple = new SimpleClass();
            input.Stuff.Add(simple);
            input.Stuff.Add(simple);

            var output = ConstructAndDeconstruct<ListClass>(input);

            Assert.NotNull(output);
            Assert.That(Object.ReferenceEquals(output.Stuff[0], output.Stuff[1]));          
        }

        [Test]
        public void DictionaryTest()
        {
            var input = new DictionaryClass();
            input.Stuff["foo"] = new SimpleClass() { IntProp = 1};
            input.Stuff["bar"] = new SimpleClass() { IntProp = 2};

            var output = ConstructAndDeconstruct<DictionaryClass>(input);

            Assert.NotNull(output);
            Assert.That(output.Stuff["foo"].IntProp == 1);
            Assert.That(output.Stuff["bar"].IntProp == 2);
        }

        [Test]
        public void NestedCustomSerializationTest()
        {
            var input = new CustomSerializationClass()
            {
                KString = "foo"
            };
            
            input.KKlazz = input;

            var output = ConstructAndDeconstruct<CustomSerializationClass>(input);
            
            Assert.NotNull(output);
            Assert.That(Object.ReferenceEquals(output, output.KKlazz));
            Assert.That(output.KString == "foo");
        }

        private T ConstructAndDeconstruct<T>(object input) where T : class
        {
            var objectGraph = ObjectGraph.ConstructGraph(input);
            return objectGraph.DeconstructGraph() as T;    
        }
    }
}
