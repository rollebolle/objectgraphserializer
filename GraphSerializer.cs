﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectGraphSerializer
{
    /// <summary>
    /// General purpose object graph serializer. Unlike .NET's built-in serializer and the WCF
    /// serializer, which are both tree serializer, this class persists the object graph. This means
    /// that references are tracked and persisted correctly, and also that it handles object-type
    /// references.
    /// 
    /// Also unlike most popular serializers, there is no need to mark classes and all thier subclasses
    /// with attributes to indicate that they should be serialized.
    ///  
    /// Current limitations:
    ///   - Only handles List and Dictionary collections
    /// </summary>
    public class GraphSerializer
    {

        public static void Serialize<T>(T @object, Stream stream)
        {
            var graph = ObjectGraph.ConstructGraph(@object);
            using (var writer = new StreamWriter(stream, Encoding.UTF8, 512, true))
            {
                foreach (var obj in graph.Objects)
                {
                    writer.WriteLine(obj.RefId);
                    writer.WriteLine(obj.Type.ToString());
                    writer.WriteLine(obj.TypeValues.Count);
                    foreach (var typeValue in obj.TypeValues)
                    {
                        writer.WriteLine(typeValue.Name);
                        writer.WriteLine(typeValue.Type.ToString());
                        writer.WriteLine(typeValue.Value.ToString());
                    }

                    writer.WriteLine();
                }
            }
        }

        public static T Deserialize<T>(Stream stream) where T : class
        {
            var graph = new ObjectGraph();
            using (var reader = new StreamReader(stream))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    if (string.IsNullOrEmpty(line))
                        continue;

                    var node = new ObjectNode
                    {
                        RefId = Convert.ToInt32(line),
                        Type = Type.GetType(reader.ReadLine())
                    };

                    var typeValueCount = Convert.ToInt32(reader.ReadLine());
                    for (var i = 0; i < typeValueCount; i++)
                    {
                        var typeValue = new TypeValue
                        {
                            Name = reader.ReadLine(), 
                            Type = Type.GetType(reader.ReadLine())
                        };

                        if (typeValue.Type.IsValueType || typeValue.Type == typeof (string))
                            typeValue.Value = Convert.ChangeType(reader.ReadLine(), typeValue.Type);
                        else if (typeof(IList).IsAssignableFrom(typeValue.Type))
                        {                            
                            var refs = reader.ReadLine().Split(new []{" "}, StringSplitOptions.RemoveEmptyEntries);
                            var refList = new ObjectGraph.ReferenceList();
                            foreach (var @ref in refs)
                            {
                                refList.References.Add(Convert.ToInt32(@ref));
                            }

                            typeValue.Value = refList;
                        }
                        else if (typeof(IDictionary).IsAssignableFrom(typeValue.Type))
                        {
                            var keyValuePair = reader.ReadLine().Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                            var refDict = new ObjectGraph.ReferenceDictionary();
                            //var keyType = typeValue.Type.GetGenericArguments()[0];

                            foreach (var kvp in keyValuePair)
                            {
                                var kvpSplit = kvp.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                                if (kvpSplit.Count() != 2)
                                    throw new Exception("Could not parse dictionary ref string");
                                
                                refDict.References[kvpSplit[0]] = Convert.ToInt32(kvpSplit[1]);
                            }

                            typeValue.Value = refDict;

                        }
                        else // plain reference
                            typeValue.Value = Convert.ToInt32(reader.ReadLine());

                        node.TypeValues.Add(typeValue);
                    }
                 
                    graph.Objects.Add(node);
                }

            }

            return graph.DeconstructGraph() as T;
        }
    }
}
