﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ObjectGraphSerializer;
using ObjectGraphSerializer.Tests;
using NUnit.Framework;
using System.Diagnostics;

namespace ObjectGraphSerializer
{
    // playground
    class Program
    {
        class Test
        {
            public int IntProp { get; set; }
        }

        static void Main(string[] args)
        {

            //var input1 = new ListClass();
            //input1.Stuff.Add(new SimpleClass() { IntProp = 1 });
            //input1.Stuff.Add(new SimpleClass() { IntProp = 2 });

            //var outputtemp0 = ConstructAndDeconstruct<ListClass>(input1);

            //ListClass output1 = null;
            //using (var stream = File.Create("hello1.txt"))
            //{
            //    GraphSerializer.Serialize(input1, stream);


            //}

            //using (var stream = File.OpenRead("hello1.txt"))
            //{
            //    output1 = GraphSerializer.Deserialize<ListClass>(stream);
            //}
            
            
            /*var input1 = new Test();

            Stopwatch sw = new Stopwatch();

            var method = input1.GetType().GetMethod("set_IntProp");
            var param = new object[] { 4 };
            sw.Start();
            
            for (int i = 0; i < 10000000; ++i)
            {
                method.Invoke(input1, param);
                //var stuff = input1.GetType().GetProperty("IntProp");
                //stuff.SetValue(input1, 4);

            }
            sw.Stop();
            Console.WriteLine("Elapsed {0}", sw.Elapsed);*/

            var input = new DictionaryClass();
            var sc = new SimpleClass() { IntProp = 1 };
            input.Stuff["foo"] = sc;
            input.Stuff["bar"] = new SimpleClass() { IntProp = 2 };
            input.Stuff["baz"] = sc;

            var outputtemp = ConstructAndDeconstruct<DictionaryClass>(input);

            DictionaryClass output = null;
            using (var stream = File.Create("hello.txt"))
            {
                GraphSerializer.Serialize(input, stream);
                
                
            }

            using (var stream = File.OpenRead("hello.txt"))
            {
                output = GraphSerializer.Deserialize<DictionaryClass>(stream);
            }

            Assert.NotNull(output);
            Assert.That(output.Stuff["foo"].IntProp == 1);
            Assert.That(output.Stuff["bar"].IntProp == 2);
        }

        private static T ConstructAndDeconstruct<T>(object input) where T : class
        {
            var objectGraph = ObjectGraph.ConstructGraph(input);
            return objectGraph.DeconstructGraph() as T;
        }
    }
}
